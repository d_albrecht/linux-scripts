#!/bin/bash
: <<'LICENSE'
This script is part of a collection of Linux scripts and is licesed under the AGPL License v3.0.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

Copyright (C) 2022  Dennis Albrecht
LICENSE
for WINDOW in $(xdotool search --name --onlyvisible .+)
do
	NAME=$(xdotool getwindowname $WINDOW)
    [[ $NAME =~ ^(Desktop)|(cinnamon(.)*)$ ]] || xdotool windowminimize $WINDOW
done
