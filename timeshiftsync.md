timeshiftsync
=============

If you have a Linux system you probably should have some sort of snapshot tool to be able to recover from any mis-configuration or failed install/update. For me that currently is `timeshift`. But timeshift is no backup in the sense that if the physical drive fails, then the snapshots won't help you recovering the system at all - depending on the severity of the drive failure.

As some form of minor backup I decided to sync my timeshift backups to a second (hdd) drive in the same system. If a power spike should ever happen, this will surely not save the data, as both drives are connected to and powered by the same PSU. This at least prevents some data loss due to wear and tear of the system drive. But technically, you can use the same approach to backup to an external system like a NAS:

Having this script in this repository is just to keep the script somewhere safe. Because over the time I had to come back to the script and tweak the parameters several time as I had messed up before. Mostly only causing wasted disk space. But some update of `rsync` itself caused the accepted parameters to change drastically. Keep that in mind when using this script, rsync will usually (in my experience) tell you that it couldn't parse some parameters and will refuse to run. If it runs once and you don't update rsync, it should (!) work all the time.

## Usage

Set the locations of both your original timeshift snapshots and the desired backup location and run the script. Optimally in some scheduled manner (for example using anacron).

With each run the script will produce a log-file with the current timestamp. You can disable this by removing the parameter, but as this usually runs in the background, having no output at all will be a problem when trouble-shooting. As these log-files can use up a lot of space, you should - either manually or scheduled - clean up the logs every now and then.