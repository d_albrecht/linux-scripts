Linux scripts
=============

Some collection of useful scripts for particular Linux systems.

## Motivation

Without any explicit intention to make this repository relevant for anyone but me, this is just a collection of scripts that are general enough that some might want to make use of them. But mostly this repository is just set up to have the scripts somewhat backed up or as some point of reference.

## Structure

Each script is accompanied by a markdown file of the same name further describing the goals and prerequisite for any of these scripts.

## Usage

Each script should explain the type of system that it is developed for, but all scripts are focused on Linux systems of sorts. Usually the scripts are written for /bin/sh (fully POSIX-compliant and only assuming POSIX-features), some might make use of specific shells like Bash.

## License
If not stated otherwise all scripts are licensed under AGPL 3.0.