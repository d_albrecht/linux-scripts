Minimize all windows (for Cinnamon)
===================================

Script to minimize all windows on a Linux machine. The current version is focused on the Cinnamon DE.

## History

As a previous long time Windows user and now new Linux user I was used to some routines that I learned in Windows that I had a hard time dropping when transitioning to Linux. One of them was the way on Windows that I got used to on how to switch to the Desktop. Most users might think that this was Win+D but this shortcut annoyed me for several reasons. First, Win+D minimized literally everything, even windows without icons in the task bar e.g. confirmation dialogues. In case those dialogues appeared behind some focused window, you had a hard time realizing, that there was a dialogue coming up at all. And they tended to pop up what seemed to be arbitrarily when de-minimzing one window. Second, Win+D is reversable by the same shortcut. Depening on the workload on my previous system (it wasn't quite the fastest hardware) I had the situation that the shortcut didn't trigger right away and pressing it again negated the first attempt. There might have been other reasons but that were the two major reasons that were the cause for me to look around for other solutions to this use case. That's when I discovered Win+M. This one minimizes only windows with an icon (therefore, it can help finding the dialogue that blocks some action if the OS doesn't want to put the dialogue up front) and is reversible by pressing Win+Shift+M. So it is reversible but only if you indent to do so.

The distro I transitioned to first was LMDE and in its most recent version at that time such a functionality wasn't available. Fortunately, on Linux much more things have an icon in the task bar so bringing back any dialogues isn't tricky at all so minimizing dialogues wasn't a problem here (in the distros and DEs I was using). So, I looked around again to find other DEs that would have it (and althought I plan to try out other distros from time to time, I intend to get comfortable on one Linux before spending my time with distro hopping) or scripts that pretent to do just this. Thing is, some scripts work too well. They seem to not play nicely with Cinnamon - the default DE of LMDE. This means that invoking those scripts minimizes windows that are windows for the system but not for the user. This includes the screensaver-"window" and desktop-"window" (the container for all the desktop-icons) and some others that I don't even know what they're needed for.

Unfortunately, I can't recall which scripts I tried out at that time.

But given that there is hardly anything on Linux that you can't customize or achieve, I used these scripts as a starting point for coming up with my own script. I mean this wasn't an overly complex goal that I tried to reach. Find all windows that are worth minimizing and invoke this command. So here you are, a script that you can bind to any keyboard shortcut that respects cinnamon's way of rendering your system. Up to this point, this script doesn't have a was of reversing, but this was never a feature on Windows that I relied on.

## Usage

This script relies on xdotool for all the xserver-related queries and commands.

On Debian based systems this should be available via
```
# apt install xdotool
```

The script does not handle any exceptions that may arise with interacting with xdotool. Otherwise, just invoke the script and see all windows to be minimized.

The script is written with Bash in mind, so compatibility with other shells might not be a given.

## Future Improvements

What I don't like about my script at the moment is at the same time what I just said wasn't necessary for me. This script tries to minimize everything that I haven't explicitly excluded. This doesn't do much harm because minimizing a minimized window changes - idealy - nothing. But the reverse of this script would de-minimize every window even if it was already minimized before invoking the script. This might be what others would want to have but isn't what Windows does. Microsoft isn't exactly the blue print for the best solutions, but given that Windows was the original I tried to port to Linux here, I might want to recreate it as good as possible.

And this already describes another flaw of my approach. I use an exclude list for windows that I personally know are difficult to get back or that the system might rely on in the future (for me, every window that starts with "cinnamon" and the "desktop"). This means that this might be a good solution for cinnamon - except for when some application calls its window "Desktop" - but would require adaption for any other DE. The ideal script would just determine if an application's window has an icon and spare anything without one. But I haven't managed to accomplish this yet.

In addition, right now this script graps all applications regardless of their assigned workspace, so even windows on unselected workspaces will be minimized. This again might be intentional but having an option to influence this behavior might be beneficial.
