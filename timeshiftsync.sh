#!/bin/sh
: <<'LICENSE'
This script is part of a collection of Linux scripts and is licesed under the AGPL License v3.0.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>

Copyright (C) 2022  Dennis Albrecht
LICENSE
TIMESHIFT_PATH="/timeshift"
SYNC_PATH="/somewhere/different"
rsync -aHuv --delete --atimes --open-noatime --log-file="$SYNC_PATH/$(date +%F_%T)" "$TIMESHIFT_PATH" "$SYNC_PATH"